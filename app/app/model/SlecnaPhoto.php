<?php

namespace App\Model;


/**
 * Description of Slecna
 *
 * @author David
 */
class SlecnaPhoto extends BaseModel
{

	private $id;
	private $slecna_id;
	private $name;
	private $origname;
	private $ord;
	private $verify;
	private $verify_date;

	public function getId()
	{
		return $this->id;
	}

	public function getSlecna_id()
	{
		return $this->slecna_id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getOrd()
	{
		return $this->ord;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setSlecna_id($slecna_id)
	{
		$this->slecna_id = $slecna_id;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setOrd($ord)
	{
		$this->ord = $ord;
	}

	public function getOrigname()
	{
		return $this->origname;
	}

	public function setOrigname($origname)
	{
		$this->origname = $origname;
	}

	function getVerify()
	{
		return $this->verify;
	}

	function getVerify_date()
	{
		return $this->verify_date;
	}

	function setVerify($verify)
	{
		$this->verify = $verify;
	}

	function setVerify_date($verify_date)
	{
		$this->verify_date = $verify_date;
	}

}

<?php

namespace App\Model;

/**
 * Description of Menu
 *
 * @author David
 */
class Price extends BaseModel {

	private $id;
	private $slecna_id;
	private $slecna;
	private $podnik_id;
	private $podnik;
	private $col0;
	private $col1;
	private $col2;
	private $col3;
	private $col4;
	private $col5;
	private $col6;
	private $col7;
	private $col8;
	private $currency = 'CZK';
	private $type_id;

	public function getId() {
		return $this->id;
	}

	public function getCol0() {
		return $this->col0;
	}

	public function getCol1() {
		return $this->col1;
	}

	public function getCol2() {
		return $this->col2;
	}

	public function getCol3() {
		return $this->col3;
	}

	public function getCol4() {
		return $this->col4;
	}

	public function getCol5() {
		return $this->col5;
	}

	public function getCol6() {
		return $this->col6;
	}

	public function getCol7() {
		return $this->col7;
	}

	public function getCol8() {
		return $this->col8;
	}

	public function getCurrency() {
		return $this->currency;
	}

	public function getType_id() {
		return $this->type_id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setCol0($col0) {
		$this->col0 = $col0;
	}

	public function setCol1($col1) {
		$this->col1 = $col1;
	}

	public function setCol2($col2) {
		$this->col2 = $col2;
	}

	public function setCol3($col3) {
		$this->col3 = $col3;
	}

	public function setCol4($col4) {
		$this->col4 = $col4;
	}

	public function setCol5($col5) {
		$this->col5 = $col5;
	}

	public function setCol6($col6) {
		$this->col6 = $col6;
	}

	public function setCol7($col7) {
		$this->col7 = $col7;
	}

	public function setCol8($col8) {
		$this->col8 = $col8;
	}

	public function setCurrency($currency) {
		$this->currency = $currency;
	}

	public function setType_id($type_id) {
		$this->type_id = $type_id;
	}

	public function getSlecna() {
		return $this->slecna;
	}

	public function getPodnik() {
		return $this->podnik;
	}

	public function setSlecna($slecna) {
		$this->slecna = $slecna;
	}

	public function setPodnik($podnik) {
		$this->podnik = $podnik;
	}

	public function getSlecna_id() {
		return $this->slecna_id;
	}

	public function getPodnik_id() {
		return $this->podnik_id;
	}

	public function setSlecna_id($slecna_id) {
		$this->slecna_id = $slecna_id;
	}

	public function setPodnik_id($podnik_id) {
		$this->podnik_id = $podnik_id;
	}
	
			public function compareObject(Price $podnik)
	{
				$diff = array();
		foreach (get_class_vars(get_class($this)) as $var => $val) {
			if (is_object($this->$var) && !($this->$var instanceof \DateTime)) {
			} elseif (is_array($this->$var)) {
				
			
			} else {
				if (!($this->$var == $podnik->$var)) {
					$diff[$var] = array('old' => $podnik->$var, 'new' => $this->$var);
				}
			}
		}
		return $diff;
	}

}

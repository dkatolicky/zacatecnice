<?php

namespace App\Model;


/**
 * Description of Slecna
 *
 * @author David
 */
class SlecnaLang extends BaseModel
{

	private $id;
	private $slecna_id;
	private $lang;
	private $nazev;
	private $jmeno;
	private $vl_jmeno;
	private $vl_tel;
	private $vl_email;
	private $vl_birthdate;
	private $nazev_podniku;
	private $vek;
	private $vyska;
	private $vaha;
	private $prsa;
	private $vlasy;
	private $oci;
	private $ochlupeni;
	private $jazyky;
	private $region;
	private $ulice;
	private $mesto;
	private $metro;
	private $telefon;
	private $email;
	private $ostatni;
	private $predstava_schuzky;
	private $mailtext;
	private $ostatni_box;
	private $header;
	private $footer;
	private $ostatni_box_price_1;
	private $ostatni_box_price_2;

	public function getPredstava_schuzky()
	{
		return $this->predstava_schuzky;
	}

	public function setPredstava_schuzky($predstava_schuzky)
	{
		$this->predstava_schuzky = $predstava_schuzky;
	}

	public function getMailtext()
	{
		return $this->mailtext;
	}

	public function setMailtext($mailtext)
	{
		$this->mailtext = $mailtext;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getSlecna_id()
	{
		return $this->slecna_id;
	}

	public function getLang()
	{
		return $this->lang;
	}

	public function getNazev()
	{
		return $this->nazev;
	}

	public function getJmeno()
	{
		return $this->jmeno;
	}

	public function getVl_jmeno()
	{
		return $this->vl_jmeno;
	}

	public function getVl_tel()
	{
		return $this->vl_tel;
	}

	public function getVl_email()
	{
		return $this->vl_email;
	}

	public function getVek()
	{
		return $this->vek;
	}

	public function getVyska()
	{
		return $this->vyska;
	}

	public function getVaha()
	{
		return $this->vaha;
	}

	public function getPrsa()
	{
		return $this->prsa;
	}

	public function getVlasy()
	{
		return $this->vlasy;
	}

	public function getOci()
	{
		return $this->oci;
	}

	public function getOchlupeni()
	{
		return $this->ochlupeni;
	}

	public function getJazyky()
	{
		return $this->jazyky;
	}

	public function getRegion()
	{
		return $this->region;
	}

	public function getUlice()
	{
		return $this->ulice;
	}

	public function getMesto()
	{
		return $this->mesto;
	}

	public function getMetro()
	{
		return $this->metro;
	}

	public function getTelefon()
	{
		return $this->telefon;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function getOstatni()
	{
		return $this->ostatni;
	}

	public function getOstatni_box()
	{
		return $this->ostatni_box;
	}

	public function getHeader()
	{
		return $this->header;
	}

	public function getFooter()
	{
		return $this->footer;
	}

	public function getOstatni_box_price_1()
	{
		return $this->ostatni_box_price_1;
	}

	public function getOstatni_box_price_2()
	{
		return $this->ostatni_box_price_2;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setSlecna_id($slecna_id)
	{
		$this->slecna_id = $slecna_id;
	}

	public function setLang($lang)
	{
		$this->lang = $lang;
	}

	public function setNazev($nazev)
	{
		$this->nazev = $nazev;
	}

	public function setJmeno($jmeno)
	{
		$this->jmeno = $jmeno;
	}

	public function setVl_jmeno($vl_jmeno)
	{
		$this->vl_jmeno = $vl_jmeno;
	}

	public function setVl_tel($vl_tel)
	{
		$this->vl_tel = $vl_tel;
	}

	public function setVl_email($vl_email)
	{
		$this->vl_email = $vl_email;
	}

	public function setVek($vek)
	{
		$this->vek = $vek;
	}

	public function setVyska($vyska)
	{
		$this->vyska = $vyska;
	}

	public function setVaha($vaha)
	{
		$this->vaha = $vaha;
	}

	public function setPrsa($prsa)
	{
		$this->prsa = $prsa;
	}

	public function setVlasy($vlasy)
	{
		$this->vlasy = $vlasy;
	}

	public function setOci($oci)
	{
		$this->oci = $oci;
	}

	public function setOchlupeni($ochlupeni)
	{
		$this->ochlupeni = $ochlupeni;
	}

	public function setJazyky($jazyky)
	{
		$this->jazyky = $jazyky;
	}

	public function setRegion($region)
	{
		$this->region = $region;
	}

	public function setUlice($ulice)
	{
		$this->ulice = $ulice;
	}

	public function setMesto($mesto)
	{
		$this->mesto = $mesto;
	}

	public function setMetro($metro)
	{
		$this->metro = $metro;
	}

	public function setTelefon($telefon)
	{
		$this->telefon = $telefon;
	}

	public function setEmail($email)
	{
		$this->email = $email;
	}

	public function setOstatni($ostatni)
	{
		$this->ostatni = $ostatni;
	}

	public function setOstatni_box($ostatni_box)
	{
		$this->ostatni_box = $ostatni_box;
	}

	public function setHeader($header)
	{
		$this->header = $header;
	}

	public function setFooter($footer)
	{
		$this->footer = $footer;
	}

	public function setOstatni_box_price_1($ostatni_box_price_1)
	{
		$this->ostatni_box_price_1 = $ostatni_box_price_1;
	}

	public function setOstatni_box_price_2($ostatni_box_price_2)
	{
		$this->ostatni_box_price_2 = $ostatni_box_price_2;
	}

	public function getVl_birthdate()
	{
		return $this->vl_birthdate;
	}

	public function getNazev_podniku()
	{
		return $this->nazev_podniku;
	}

	public function setVl_birthdate($vl_birthdate)
	{
		$this->vl_birthdate = $vl_birthdate;
	}

	public function setNazev_podniku($nazev_podniku)
	{
		$this->nazev_podniku = $nazev_podniku;
	}

	public function compareObject(SlecnaLang $slecnaLang)
	{
		$diff = array();
		foreach (get_class_vars(get_class($this)) as $var => $val) {
			if (is_object($this->$var) && !($this->$var instanceof \DateTime)) {
//				$this->$var->compareObject($slecna->$var);
			} elseif (is_array($this->$var)) {
				
			} else {
				if (!($this->$var == $slecnaLang->$var)) {
					$diff[$var] = array('old' => $slecnaLang->$var, 'new' => $this->$var);
				}
			}
		}
		return $diff;
	}

}

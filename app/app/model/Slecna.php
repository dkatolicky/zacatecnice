<?php

namespace App\Model;


/**
 * Description of Slecna
 *
 * @author David
 */
class Slecna extends BaseModel
{

	private $id;
	private $inserted;
	private $url;
	private $mainfoto;
	private $mainfotoland;
	private $active;
	private $approved = false;
	private $verified = false;
	private $contact_verified = false;
	private $ord;
	private $podnik_id;
	private $podnik;
	private $multiprofil;
	private $multi1;
	private $multi2;
	private $multi3;
	private $multi4;
	private $specialprice;
	private $price_privat;
	private $price_escort_in_prague;
	private $price_escort_out_prague;
	private $price_escort_out_prague_more31;
	private $photo1;
	private $photo2;
	private $photo3;
	private $photo4;
	private $photo5;
	private $langs = array();
	private $serviceTypes = array();
	private $photos = array();

	function getInserted()
	{
		return $this->inserted;
	}

	function setInserted($inserted)
	{
		$this->inserted = $inserted;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getUrl()
	{
		return $this->url;
	}

	public function getMainfoto()
	{
		return $this->mainfoto;
	}

	public function getActive()
	{
		return $this->active;
	}

	public function getOrd()
	{
		return $this->ord;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setUrl($url)
	{
		$this->url = $url;
	}

	public function setMainfoto($mainfoto)
	{
		$this->mainfoto = $mainfoto;
	}

	public function setActive($active)
	{
		$this->active = $active;
	}

	public function setOrd($ord)
	{
		$this->ord = $ord;
	}

	public function getLangs()
	{
		return $this->langs;
	}

	public function setLangs($langs)
	{
		$this->langs = $langs;
	}

	public function addLang($lang, $data)
	{
		$this->langs[$lang] = $data;
	}

	public function getLang($lang)
	{
		if (!isset($this->langs[$lang])) {
			$this->addLang($lang, $this->context->slecnaLangDAO->getGirlLang($lang, $this->getId()));
		}
		return $this->langs[$lang];
	}

	public function getServiceTypes()
	{
		if (count($this->serviceTypes) == 0) {
			$this->serviceTypes = $this->getPodnik()->serviceType;
			$services = $this->context->slecnaDAO->getGirlOfferedServices($this);

			foreach ($this->serviceTypes as $typeKey => $typeValue) {
				foreach ($typeValue->services as $serviceKey => $serviceValue) {
					if (in_array($serviceKey, $services)) {
						$serviceValue->setOffered(true);
					}
				}
			}
		}

		return $this->serviceTypes;
	}

	public function setServiceTypes($serviceTypes)
	{
		$this->serviceTypes = $serviceTypes;
	}

	public function addService($serviceType)
	{
		$this->serviceTypes[$service->id] = $service;
	}

	public function getPhotos()
	{
		if (count($this->photos) == 0) {
			$this->photos = $this->context->slecnaPhotoDAO->getAllByGirlId($this->getId());
		}
		return $this->photos;
	}

	public function setPhotos($photos)
	{
		$this->photos = $photos;
	}

	public function getFirstPhoto()
	{
		$keys = array_keys($this->photos);
		if (count($keys) > 0) {
			return $this->photos[$keys[0]];
		} else {
			return null;
		}
	}

	public function getPodnik_id()
	{
		return $this->podnik_id;
	}

	public function getPodnik()
	{
		if (!isset($this->podnik)) {
			$this->podnik = $this->context->podnikDAO->get($this->getPodnik_id());
		}
		return $this->podnik;
	}

	public function setPodnik_id($podnik_id)
	{
		$this->podnik_id = $podnik_id;
	}

	public function setPodnik($podnik)
	{
		$this->podnik = $podnik;
	}

	public function getMultiprofil()
	{
		return $this->multiprofil;
	}

	public function getMulti1()
	{
		return $this->multi1;
	}

	public function getMulti2()
	{
		return $this->multi2;
	}

	public function getMulti3()
	{
		return $this->multi3;
	}

	public function getMulti4()
	{
		return $this->multi4;
	}

	public function setMultiprofil($multiprofil)
	{
		$this->multiprofil = $multiprofil;
	}

	public function setMulti1($multi1)
	{
		$this->multi1 = $multi1;
	}

	public function setMulti2($multi2)
	{
		$this->multi2 = $multi2;
	}

	public function setMulti3($multi3)
	{
		$this->multi3 = $multi3;
	}

	public function setMulti4($multi4)
	{
		$this->multi4 = $multi4;
	}

	public function getSpecialprice()
	{
		return $this->specialprice;
	}

	public function setSpecialprice($specialprice)
	{
		$this->specialprice = $specialprice;
	}

	public function getPrice_privat()
	{
		if ($this->price_privat === NULL) {
			$this->setPrice_privat($this->context->priceDAO->getBySlecna($this, 1));
		}
		return $this->price_privat;
	}

	public function getPrice_escort_in_prague()
	{
		if ($this->price_escort_in_prague === NULL) {
			$this->setPrice_escort_in_prague($this->context->priceDAO->getBySlecna($this, 2));
		}
		return $this->price_escort_in_prague;
	}

	public function getPrice_escort_out_prague()
	{
		if ($this->price_escort_out_prague === NULL) {
			$this->setPrice_escort_out_prague($this->context->priceDAO->getBySlecna($this, 3));
		}
		return $this->price_escort_out_prague;
	}

	public function getPrice_escort_out_prague_more31()
	{
		if ($this->price_escort_out_prague_more31 === NULL) {
			$this->setPrice_escort_out_prague_more31($this->context->priceDAO->getBySlecna($this, 4));
		}
		return $this->price_escort_out_prague_more31;
	}

	public function setPrice_privat($price_privat)
	{
		$this->price_privat = $price_privat;
	}

	public function setPrice_escort_in_prague($price_escort_in_prague)
	{
		$this->price_escort_in_prague = $price_escort_in_prague;
	}

	public function setPrice_escort_out_prague($price_escort_out_prague)
	{
		$this->price_escort_out_prague = $price_escort_out_prague;
	}

	public function setPrice_escort_out_prague_more31($price_escort_out_prague_more31)
	{
		$this->price_escort_out_prague_more31 = $price_escort_out_prague_more31;
	}

	public function getMainfotoland()
	{
		return $this->mainfotoland;
	}

	public function setMainfotoland($mainfotoland)
	{
		$this->mainfotoland = $mainfotoland;
	}

	public function getPhoto1()
	{
		return $this->photo1;
	}

	public function getPhoto2()
	{
		return $this->photo2;
	}

	public function getPhoto3()
	{
		return $this->photo3;
	}

	public function getPhoto4()
	{
		return $this->photo4;
	}

	public function getPhoto5()
	{
		return $this->photo5;
	}

	public function setPhoto1($photo1)
	{
		$this->photo1 = $photo1;
	}

	public function setPhoto2($photo2)
	{
		$this->photo2 = $photo2;
	}

	public function setPhoto3($photo3)
	{
		$this->photo3 = $photo3;
	}

	public function setPhoto4($photo4)
	{
		$this->photo4 = $photo4;
	}

	public function setPhoto5($photo5)
	{
		$this->photo5 = $photo5;
	}

	function getApproved()
	{
		return $this->approved;
	}

	function setApproved($approved)
	{
		$this->approved = $approved;
	}

	public function getPrices()
	{
		$ret = new \stdClass();
		$ret->price_privat = new \stdClass();
		$ret->price_escort_in_prague = new \stdClass();
		$ret->price_escort_out_prague = new \stdClass();
		$ret->price_escort_out_prague_more31 = new \stdClass();

		if ($this->podnik instanceof Podnik) {
			foreach (array('price_privat', 'price_escort_in_prague', 'price_escort_out_prague', 'price_escort_out_prague_more31') as $type) {
				for ($i = 0; $i <= 8; $i++) {
					$ret->$type->{'col' . $i} = $this->podnik->$type->{'col' . $i};
				}
			}
		}
		if ($this->specialprice) {
			foreach (array('price_privat', 'price_escort_in_prague', 'price_escort_out_prague', 'price_escort_out_prague_more31') as $type) {
				for ($i = 0; $i <= 8; $i++) {
					if (!empty($this->$type->{'col' . $i})) {
						$ret->$type->{'col' . $i} = $this->$type->{'col' . $i};
					}
				}
			}
		}
		return $ret;
	}

	public function getEurPrices()
	{
		$ret = new \stdClass();
		$ret->price_privat = new \stdClass();
		$ret->price_escort_in_prague = new \stdClass();
		$ret->price_escort_out_prague = new \stdClass();
		$ret->price_escort_out_prague_more31 = new \stdClass();

		if ($this->podnik instanceof Podnik) {
			foreach (array('price_privat', 'price_escort_in_prague', 'price_escort_out_prague', 'price_escort_out_prague_more31') as $type) {
				for ($i = 0; $i <= 8; $i++) {
					$ret->$type->{'col' . $i} = str_replace('.', '', $this->podnik->$type->{'col' . $i}) / $this->podnik->course;
				}
			}
		}
		if ($this->specialprice) {
			foreach (array('price_privat', 'price_escort_in_prague', 'price_escort_out_prague', 'price_escort_out_prague_more31') as $type) {
				for ($i = 0; $i <= 8; $i++) {
					if (!empty($this->$type->{'col' . $i})) {
						$ret->$type->{'col' . $i} = str_replace('.', '', $this->$type->{'col' . $i}) / $this->podnik->course;
					}
				}
			}
		}
		return $ret;
	}

	function getVerified()
	{
		return $this->verified;
	}

	function getContact_verified()
	{
		return $this->contact_verified;
	}

	function setVerified($verified)
	{
		$this->verified = $verified;
	}

	function setContact_verified($contact_verified)
	{
		$this->contact_verified = $contact_verified;
	}

	public function compareObject(Slecna $slecna)
	{
		$diff = array();
		foreach (get_class_vars(get_class($this)) as $var => $val) {
			if (is_object($this->$var) && !($this->$var instanceof \DateTime) && $var != 'context') {
				$svar = 'get'.ucfirst($var);
				$x = $this->$var->compareObject($slecna->$svar());
				if (!empty($x)) {
					$diff[$var] = $x;
				}
			} elseif (is_array($this->$var)) {
				switch ($var) {
					case 'langs':

						foreach ($this->$var as $var2 => $val2) {
						$thisLang = $this->getLang($var2);
						$remoteLang = $slecna->getLang($var2);
//							$x = $this->{$var}[$var2]->compareObject($slecna->{$var}[$var2]);
							$x = $thisLang->compareObject($remoteLang);
							if (!empty($x)) {
								$diff[$var][$var2] = $x;
							}
						}
						break;
					case 'serviceTypes':
						$old = array();
						foreach ($slecna->$var as $val) {
							foreach ($val->services as $service) {
								if ($service->offered == true) {
									$old[] = $service->description;
								}
							}
						}
						$new = array();
						foreach ($this->$var as $val) {
							foreach ($val->services as $service) {
								if ($service->offered == true) {
									$new[] = $service->description;
								}
							}
						}
						if (count(array_diff($new, $old)) > 0 || count(array_diff($old, $new)) > 0) {
							$diff[$var] = array('old' => $old, 'new' => $new);
						}
						break;
					case 'photos':
						//TODO
						break;
					default:
						break;
				}
			} else {
				if (!($this->$var == $slecna->$var)) {
					$diff[$var] = array('old' => $slecna->$var, 'new' => $this->$var);
				}
			}
		}
		return $diff;
	}

	public function hasPhotos()
	{
		if (count($this->photos) == 0) {
			return FALSE;
		}
		return TRUE;
	}

}

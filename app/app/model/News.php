<?php

namespace App\Model;

/**
 * Description of News
 *
 * @author David
 */
class News extends BaseModel {

	private $id;
	private $time;
	private $description;
	private $text;
	private $description_en;
	private $text_en;
	private $active;
	private $ord;
	private $lang;
	private $web_id;

	public function getId() {
		return $this->id;
	}

	public function getTime() {
		return $this->time;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getText() {
		return $this->text;
	}

	public function getDescription_en() {
		return $this->description_en;
	}

	public function getText_en() {
		return $this->text_en;
	}

	public function getActive() {
		return $this->active;
	}

	public function getOrd() {
		return $this->ord;
	}

	public function getLang() {
		return $this->lang;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setTime($time) {
		$this->time = $time;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setText($text) {
		$this->text = $text;
	}

	public function setDescription_en($description_en) {
		$this->description_en = $description_en;
	}

	public function setText_en($text_en) {
		$this->text_en = $text_en;
	}

	public function setActive($active) {
		$this->active = $active;
	}

	public function setOrd($ord) {
		$this->ord = $ord;
	}

	public function setLang($lang) {
		$this->lang = $lang;
	}

	function getWeb_id() {
		return $this->web_id;
	}

	function setWeb_id($web_id) {
		$this->web_id = $web_id;
	}

}

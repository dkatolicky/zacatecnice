<?php

namespace App\Model;

/**
 * Description of Menu
 *
 * @author David
 */
class Web extends BaseModel {

	private $id;
	private $name;
	private $url;
	private $note;

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getUrl() {
		return $this->url;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setUrl($url) {
		$this->url = $url;
	}
	public function getNote()
	{
		return $this->note;
	}

	public function setNote($note)
	{
		$this->note = $note;
	}



}

<?php

namespace App\Model;

/**
 * Description of Page
 *
 * @author David
 */
class Page extends BaseModel {

	private $id;
	private $name;
	private $content;
	private $lang;
	private $name_en;
	private $content_en;
	private $type;
	private $component;
	private $header;
	private $header_en;
	private $footer;
	private $footer_en;

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getContent() {
		return $this->content;
	}

	public function getName_en() {
		return $this->name_en;
	}

	public function getContent_en() {
		return $this->content_en;
	}

	public function getType() {
		return $this->type;
	}

	public function getComponent() {
		return $this->component;
	}

	public function getHeader() {
		return $this->header;
	}

	public function getHeader_en() {
		return $this->header_en;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setContent($content) {
		$this->content = $content;
	}

	public function setName_en($name_en) {
		$this->name_en = $name_en;
	}

	public function setContent_en($content_en) {
		$this->content_en = $content_en;
	}

	public function setType($type) {
		$this->type = $type;
	}

	public function setComponent($component) {
		if ($component == "") {
			$this->component = null;
		} else {
			$this->component = $component;
		}
	}

	public function setHeader($header) {
		$this->header = $header;
	}

	public function setHeader_en($header_en) {
		$this->header_en = $header_en;
	}

	public function getLang() {
		return $this->lang;
	}

	public function setLang($lang) {
		$this->lang = $lang;
	}

	public function getFooter() {
		return $this->footer;
	}

	public function getFooter_en() {
		return $this->footer_en;
	}

	public function setFooter($footer) {
		$this->footer = $footer;
	}

	public function setFooter_en($footer_en) {
		$this->footer_en = $footer_en;
	}

}

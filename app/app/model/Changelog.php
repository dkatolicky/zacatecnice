<?php

namespace App\Model;


/**
 * Description of Menu
 *
 * @author David
 */
class Changelog extends BaseModel
{

	const TYPE_SLECNA = 'slecna';
	const TYPE_CALENDAR = 'calendar';

	private $id;
	private $slecna_id;
	private $type;
	private $log;
	private $datetime;
	private $confirm_admin = 0;
	private $confirm_subadmin = 0;
	private $user_id;

	public function getId()
	{
		return $this->id;
	}

	public function getSlecna_id()
	{
		return $this->slecna_id;
	}

	public function getType()
	{
		return $this->type;
	}

	public function getLog()
	{
		return $this->log;
	}

	public function getDatetime()
	{
		return $this->datetime;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setSlecna_id($slecna_id)
	{
		$this->slecna_id = $slecna_id;
	}

	public function setType($type)
	{
		$this->type = $type;
	}

	public function setLog($log)
	{
		$this->log = $log;
	}

	public function setDatetime($datetime)
	{
		$this->datetime = $datetime;
	}

	public function getConfirm_admin()
	{
		return $this->confirm_admin;
	}

	public function getConfirm_subadmin()
	{
		return $this->confirm_subadmin;
	}

	public function setConfirm_admin($confirm_admin)
	{
		$this->confirm_admin = $confirm_admin;
	}

	public function setConfirm_subadmin($confirm_subadmin)
	{
		$this->confirm_subadmin = $confirm_subadmin;
	}

	function getUser_id()
	{
		return $this->user_id;
	}

	function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

}

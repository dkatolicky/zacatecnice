<?php

namespace App\Model;

/**
 * Description of Menu
 *
 * @author David
 */
class Menu extends BaseModel {

	private $id;
	private $item;
	private $name;
	private $name_en;
	private $lang;
	private $page_id;
	private $page;
	private $visible;
	private $ord;
	private $web_id;

	public function getId() {
		return $this->id;
	}

	public function getItem() {
		return $this->item;
	}

	public function getName() {
		return $this->name;
	}

	public function getName_en() {
		return $this->name_en;
	}

	public function getLang() {
		return $this->lang;
	}

	public function getPage_id() {
		return $this->page_id;
	}

	public function getPage() {
		return $this->page;
	}

	public function getVisible() {
		return $this->visible;
	}

	public function getOrd() {
		return $this->ord;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setItem($item) {
		$this->item = $item;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setName_en($name_en) {
		$this->name_en = $name_en;
	}

	public function setLang($lang) {
		$this->lang = $lang;
	}

	public function setPage_id($page_id) {
		$this->page_id = $page_id;
	}

	public function setPage($page) {
		$this->page = $page;
	}

	public function setVisible($visible) {
		$this->visible = $visible;
	}

	public function setOrd($ord) {
		$this->ord = $ord;
	}

	public function getWeb_id() {
		return $this->web_id;
	}

	public function setWeb_id($web_id) {
		$this->web_id = $web_id;
	}

}

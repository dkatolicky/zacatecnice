<?php

namespace App\dao;

/**
 * Description of PageDAO
 *
 * @author David
 */
class PageDAO extends BaseDAO {

	public $table = 'page';
	public $model = 'App\Model\Page';

	public function save(\App\Model\Page $model) {
		$data = array(
			'name' => $model->name,
			'content' => $model->content,
			'lang' => $model->lang,
			'name_en' => $model->name_en,
			'content_en' => $model->content_en,
			'type' => $model->type,
			'component' => $model->component,
			'header' => $model->header,
			'header_en' => $model->header_en,
			'footer' => $model->footer,
			'footer_en' => $model->footer_en,
		);
		return parent::saveData($data, $model);
	}

}

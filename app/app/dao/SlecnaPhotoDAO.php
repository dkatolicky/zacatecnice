<?php

namespace App\dao;

/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class SlecnaPhotoDAO extends BaseDAO {

	public $table = 'slecnaphoto';
	public $model = 'App\Model\SlecnaPhoto';

	public function getAllByGirlId($slecna_id) {
		$ret = array();
		foreach ($this->db->table($this->table)->where('slecna_id', $slecna_id)->order('ord') as $row) {
			$ret[$row->id] = new $this->model($row->toArray());
		}
		return $ret;
	}

	public function deleteAllByGirlId($slecna_id) {
		$this->db->table($this->table)->where('slecna_id', $slecna_id)->delete();
	}

	public function getMaxPhotoByGirl($slecna_id) {
		return $this->db->table($this->table)->where('slecna_id', $slecna_id)->max('ord');
	}

	public function save(\App\Model\SlecnaPhoto $model) {
		$data = array(
				'slecna_id' => $model->slecna_id,
				'name' => $model->name,
				'origname' => $model->origname,
				'ord' => $model->ord
		);
		return parent::saveData($data, $model);
	}

	public function orderPhotoByName($slecna_id) {
		$i = 0;
		foreach ($this->db->table($this->table)->where('slecna_id', $slecna_id)->order('origname') as $row) {
			$i++;
			$this->db->table($this->table)->where('id',$row->id)->update(array('ord'=>$i));
		}
	}
	
	public function verify($id)
	{
		$this->db->table($this->table)->wherePrimary($id)->update(array('verify'=>1,'verify_date'=>new \Nette\DateTime()));
	}
	
	public function unverify($id)
	{
		$this->db->table($this->table)->wherePrimary($id)->update(array('verify'=>0,'verify_date'=>null));
	}

}

<?php

namespace App\dao;

/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class PodnikDAO extends BaseDAO {

	public $table = 'podnik';
	public $model = 'App\Model\Podnik';

	public function save(\App\Model\Podnik $model) {
		$data = array(
				'active' => $model->active,
				'url' => $model->url,
				'name' => $model->name,
				'name_en' => $model->name_en,
				'ceny_popis' => $model->ceny_popis,
				'ceny_popis_en' => $model->ceny_popis_en,
				'ceny_popis2' => $model->ceny_popis2,
				'ceny_popis2_en' => $model->ceny_popis2_en,
				'course' => $model->course,
				'note' => $model->note,
		);
		$this->db->table('podnik_servicetype')->where('podnik_id', $model->id)->delete();
		$insert = array();
		foreach ($model->serviceType as $st) {
			$insert[] = array('podnik_id' => $model->id, 'servicetype_id' => $st->id);
		}
		if (count($insert) > 0) {
			$this->db->table('podnik_servicetype')->insert($insert);
		}

		$model = parent::saveData($data, $model);
		$model->setPrice_privat($this->content->getService('priceDAO')->save($model->price_privat));
		$model->setPrice_escort_in_prague($this->content->getService('priceDAO')->save($model->price_escort_in_prague));
		$model->setPrice_escort_out_prague($this->content->getService('priceDAO')->save($model->price_escort_out_prague));
		$model->setPrice_escort_out_prague_more31($this->content->getService('priceDAO')->save($model->price_escort_out_prague_more31));
		$model->setServiceType($this->content->getService('serviceTypeDAO')->getAllByPodnik($model->id));
		return $model;
	}

	public function getAll(array $where = array(),$order = null) {
		$podniky = parent::getAll();
		foreach ($podniky as $podnik) {
			$podnik = $this->fillData($podnik);
		}
		return $podniky;
	}

	public function get($id = null, array $where = null) {
		$podnik = parent::get($id, $where);
		$podnik = $this->fillData($podnik);
		return $podnik;
	}

	public function fillData(\App\Model\Podnik $podnik) {
		$podnik->setContext($this->content);
		
		return $podnik;
	}

	/**
	 * Vrati vsechny podniky fetchnute do pole
	 * @param string $key
	 * @param string $value
	 * @return array
	 */
	public function getAllFetchPairs($key, $value) {
		return $this->db->table($this->table)->fetchPairs($key, $value);
	}
	
	public function getByName($podnik_name){
		return $this->get(null,array('name'=>$podnik_name));
	}

}

<?php

namespace App\dao;

use Nette;


/**
 * Description of BaseDao
 *
 * @author David
 */
class BaseDAO extends Nette\Object
{

	protected $db;
	protected $content;

	public function __construct(Nette\Database\Context $db, Nette\DI\Container $content)
	{
		$this->db = $db;
		$this->content = $content;
	}

	public function get($id = null, array $where = null)
	{
		if ($id !== null) {
			$x = $this->db->table($this->table)->wherePrimary($id)->fetch();
			if ($x) {
				return new $this->model($x->toArray());
			} else {
				throw new Nette\ArgumentOutOfRangeException();
			}
		} elseif ($where !== null) {
			$x = $this->db->table($this->table)->where($where)->fetch();
			if ($x) {
				return new $this->model($x->toArray());
			} else {
				throw new Nette\ArgumentOutOfRangeException();
			}
		}
	}

	public function getAll(array $where = array(), $order = null)
	{
		$ret = array();
		$rows = $this->db->table($this->table)->where($where);
		if ($order) {
			$rows->order($order);
		}
		foreach ($rows as $row) {
			$ret[$row->id] = new $this->model($row->toArray());
		}
		return $ret;
	}

	public function getAllActive()
	{
		$ret = array();
		foreach ($this->db->table($this->table)->where('active', true) as $row) {
			$ret[$row->id] = new $this->model($row->toArray());
		}
		return $ret;
	}

	public function delete($id)
	{
		$this->db->table($this->table)->wherePrimary($id)->delete();
	}

	public function saveData(array $data, $model)
	{
		if (is_numeric($model->id)) {
			$this->db->table($this->table)->where('id', $model->id)->update($data);
		} else {
			$_row = $this->db->table($this->table)->insert($data);
			$model->setId($_row->id);
		}
		return $model;
	}

	public function exist($id)
	{
		if (isset($this->{$this->table})) {
			if (isset($this->{$this->table}[$id])) {
				return $this->{$this->table}[$id];
			}
		}
		return false;
	}

	public function cache($id, $object)
	{
		if (isset($this->{$this->table})) {
			$this->{$this->table}[$id] = $object;
		}
	}

	public function getPairs($key, $value)
	{

		$ret = $this->db->table($this->table)->fetchPairs($key, $value);
		return $ret;
	}

}

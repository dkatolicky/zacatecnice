<?php

namespace App\dao;

/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class ServiceDAO extends BaseDAO {

	public $table = 'service';
	public $model = 'App\Model\Service';

		public function getAllByServiceType($serviceType) {
		$ret = array();
		$services = array();
		foreach ($this->db->table($this->table)->where('servicetype_id',$serviceType)->order('ord,id') as $row) {
			$ret[$row->id] = new $this->model($row->toArray());
		}
		return $ret;
	}



}



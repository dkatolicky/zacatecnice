<?php

namespace App\dao;

use Nette;
use App\Model\CalendarRow;
use \Exception;


/**
 * Description of CalendarDAO
 *
 * @author Pali Bazant <pali.bazant@gmail.com>
 */
class CalendarDAO extends BaseDAO
{

	public $table = 'slecnacalendar';
	public $model = 'App\Model\Calendar';

	/**
	 * vrati pole timestampu, kdy slecna pracuje nebo nepracuje
	 * timestamp=>boolean 0=nepracuje, 1=pracuje
	 * pole ma jako indexy timestamp_idSlecna
	 * 
	 * @param int $idSlecna \Model\Slecna $slecna->id
	 * @param int $nextDays
	 * @return int
	 */
	public function getGirlCalendarAdmin($idSlecna, $nextDays = 14)
	{
		$ret = array();
		foreach ($this->createCalendar($nextDays) as $day) {
			$ret[$day . '_' . $idSlecna] = 0;
		}
		foreach ($this->db->table($this->table)->select('*')->where('slecna_id', $idSlecna)->order('date') as $day) {
			$timestamp = $day->date->getTimestamp();
			if (isset($ret[$timestamp . '_' . $idSlecna])) {
				$ret[$timestamp . '_' . $idSlecna] = 1;
			}
		}

		return $ret;
	}

	/**
	 * vrati pole timestampu, kdy slecna pracuje nebo nepracuje
	 * timestamp=>boolean 0=nepracuje, 1=pracuje
	 * pole ma jako indexy timestamp
	 * 
	 * @param int $idSlecna \Model\Slecna $slecna->id
	 * @param int $nextDays
	 * @return int 
	 */
	public function getGirlCalendar($idSlecna, $nextDays = 14)
	{
		$ret = array();
		foreach ($this->createCalendar($nextDays) as $day) {
			$ret[$day] = 0;
		}
		foreach ($this->db->table($this->table)->select('*')->where('slecna_id', $idSlecna)->where('online', '1')->order('date') as $day) {
			$timestamp = $day->date->getTimestamp();
			if (isset($ret[$timestamp])) {
				$ret[$timestamp] = 1;
			}
		}

		return $ret;
	}

	/**
	 * vrati pole timestampu nasledujicich dnu dle zadaneho parametru
	 * 
	 * @param type $nextDays
	 * @return array
	 */
	public function createCalendar($nextDays = 7)
	{
		$ret = array();
		$i = 0;
		if (date('H') <= 4) {
			$i--;
			$nextDays--;
		}

//		for ($i; $i < $nextDays; $i++) {
//			$ret[] = mktime(0, 0, 0, date('n'), date('j') + $i, date('Y'));
//		}
		$today = new \Nette\DateTime;
		$today->setTime(0, 0, 0);
		if ($i < 0) {
			$today->modify('-1 day');
		}
		for ($i; $i < $nextDays; $i++) {
			$ret[] = $today->format('Y-m-d');
			$today = $today->modify('+1 day');
		}
		return $ret;
	}

	/**
	 * Ulozeni zaznamu kalendare holek do databaze
	 * 
	 * Ocekava pole ve formatu idholky[timestampy]
	 * array (2)
	 * 	1 => array (2)
	 * 		0 => "1363129200" (10)
	 * 		1 => "1363647600" (10)
	 * 	2 => array (1)
	 * 		0 => "1363388400" (10)
	 * 
	 * @param type $todb
	 */
	public function saveCalendar($todb)
	{
		$dneska = mktime(0, 0, 0, date('n'), date('j'), date('Y'));
		$datedb = new Nette\DateTime();
		$this->db->query('delete from slecnacalendar where `date`>=FROM_UNIXTIME(?)', $dneska);
		foreach ($todb as $slecnaid => $truetimestamps) {
			foreach ($truetimestamps as $truetimestamp) {
				$this->db->table($this->table)->insert(array('slecna_id' => $slecnaid, 'date' => $datedb->from($truetimestamp)));
			}
		}
	}

	public function getCalendarDay($timestamp)
	{
		
	}

	/**
	 * Vraci citelnou skratku dne s dnes a zitra
	 * @param unix timestamp $timestamp
	 * @return string
	 * @throws Exception 
	 */
	public function timestampToDayWithTomorrow($timestamp)
	{
		$dnes = mktime(0, 0, 0, date('n'), date('j'), date('Y'));
		$zitra = mktime(0, 0, 0, date('n'), date('j') + 1, date('Y'));

		if ($timestamp >= $dnes) {
			if ($timestamp == $dnes) {
				return "dnes";
			} elseif ($timestamp == $zitra) {
				return "zítra";
			} else {
				return $this->timestampToDay($timestamp);
			}
		} else {
			return $this->timestampToDay($timestamp);
		}
	}

	/**
	 * Vraci citelnou skratku dne
	 * @param unix timestamp $timestamp
	 * @return string 
	 */
	public function timestampToDay($timestamp)
	{
		$day = new \Nette\DateTime($timestamp);
		switch ($day->format('N')) {
			case 1: $n = "Pondělí ";
				break;
			case 2: $n = "Úterý ";
				break;
			case 3: $n = "Středa ";
				break;
			case 4: $n = "Čvrtek ";
				break;
			case 5: $n = "Pátek ";
				break;
			case 6: $n = "Sobota ";
				break;
			case 7: $n = "Neděle ";
				break;
		}

		return $n;
	}

	/**
	 * Vraci kalendar pro aktualni tyden
	 * @return array klic = den hodnota = id slecen
	 */
	public function getOneWeek()
	{
		$days = $this->getNowPlusSevenDays();
		$return = array();
		foreach ($days as $den) {
			$slecny = $this->db->query("select * from slecnacalendar as sc join slecna as s on s.id=sc.slecna_id where s.active=true and date_format(sc.date,'%d-%m-%Y') = date_format(FROM_UNIXTIME('$den'),'%d-%m-%Y')");
			if ($slecny !== false) {
				$return[$den] = array();
				foreach ($slecny as $slecna) {
					array_push($return[$den], $slecna->slecna_id);
				}
			} else {
				$return[$den] = false;
			}
		}
		return $return;
	}

	/**
	 * Generuje pole timestampu pre aktualny tyden
	 * @param type $timestamp
	 * @return array
	 */
	public function getDaysInWeek($timestamp)
	{
		$monday = idate('w', $timestamp) == 1 ? $timestamp : strtotime("last Monday", $timestamp);

		$days = array();
		for ($i = 0; $i < 7; ++$i) {
			$days[$i] = strtotime('+' . $i . ' days', $monday);
		}
		return $days;
	}

	public function getNowPlusSevenDays($numDays = 7)
	{
		$days = array();
		for ($i = 0; $i < $numDays; ++$i) {
			array_push($days, mktime(0, 0, 0, date("n"), date("d") + $i, date("Y")));
		}

		return $days;
	}

	/**
	 * Vraci citelny nazev dne v tydnu
	 * @param timestamp $date
	 * @param string $lang
	 * @return string
	 */
	public function getNameOfDate($date, $lang)
	{
		if ($lang == "en") {
			return date("l", $date);
		} else {
			switch (date('N', $date)) {
				case 1: $n = "Pondelí";
					break;
				case 2: $n = "Úterý";
					break;
				case 3: $n = "Středa";
					break;
				case 4: $n = "Čtvrtek";
					break;
				case 5: $n = "Pátek";
					break;
				case 6: $n = "Sobota";
					break;
				case 7: $n = "Neděle";
					break;
			}

			return $n;
		}
	}

	/**
	 * vrati pole timestampu, kdy slecna pracuje nebo nepracuje
	 * timestamp=>boolean 0=nepracuje, 1=pracuje
	 * pole ma jako indexy timestamp_idSlecna
	 * 
	 * @param int $idSlecna \Model\Slecna $slecna->id
	 * @param int $nextDays
	 * @return int
	 */
	public function getGirlCalendarAdminEdited($idSlecna, $nextDays = 14, $daysBefore = null)
	{
		$ret = array();
		foreach ($this->createCalendar($nextDays) as $day) {
			$ret[$day] = array(
					'online' => 0,
					'cely_den_online_note' => '',
					'na_dotaz_zavolanim' => 0,
					'na_dotaz_pres_sms' => 0,
					'muze_od' => '',
					'muze_do' => '',
					'cas_dojezdu_ke_klientovi_note' => '',
					'cas_dojezdu_do_prahy_note' => '',
					'divka_je_v_dany_den_v' => '',
					'note' => '',
			);
		}
		$date = new \Nette\DateTime();
		$date->setTime(0, 0, 0);
		if ($daysBefore !== null) {
			$date->modify('- ' . $daysBefore . ' days');
		}

		/**
		 * @todo muze od a muze do je timestamp nebu muize byt
		 */
		foreach ($this->db->table($this->table)->select('*')->where('slecna_id', $idSlecna)->where('date >= ?', $date)->order('date') as $day) {

			//Hack koli posunu casu +-1hod
//			$timestamp = Nette\DateTime::from($day->date->getTimestamp());
//			$timestamp = strtotime($timestamp->format('Y-m-d'));
			$timestamp = $day->date->format('Y-m-d');
			if (isset($ret[$timestamp])) {
				$ret[$timestamp]['online'] = $day->online;
				$ret[$timestamp]['cely_den_online_note'] = $day->cely_den_online_note;
				$ret[$timestamp]['na_dotaz_zavolanim'] = $day->na_dotaz_zavolanim;
				$ret[$timestamp]['na_dotaz_pres_sms'] = $day->na_dotaz_pres_sms;
				$ret[$timestamp]['muze_od'] = $day->muze_od;
				$ret[$timestamp]['muze_do'] = $day->muze_do;
				$ret[$timestamp]['cas_dojezdu_ke_klientovi_note'] = $day->cas_dojezdu_ke_klientovi_note;
				$ret[$timestamp]['cas_dojezdu_do_prahy_note'] = $day->cas_dojezdu_do_prahy_note;
				$ret[$timestamp]['divka_je_v_dany_den_v'] = $day->divka_je_v_dany_den_v;
				$ret[$timestamp]['note'] = $day->note;
			}
		}

		return $ret;
	}

	/**
	 * vrati kdy slecna pracuje nebo nepracuje + notes
	 * timestamp=>boolean 0=nepracuje, 1=pracuje
	 * 
	 * @param int $idSlecna \Model\Slecna $slecna->id
	 * @param int $day
	 * @return int
	 */
	public function getGirlCalendarAdminEditedByDay($idSlecna, $day)
	{
		$ret = array();
		$row = $this->db->table($this->table)->select('*')->where(array('slecna_id' => $idSlecna, 'date' => $day))->fetch();
		$ret['online'] = $row->online;
		$ret['cely_den_online_note'] = $row->cely_den_online_note;
		$ret['na_dotaz_zavolanim'] = $row->na_dotaz_zavolanim;
		$ret['na_dotaz_pres_sms'] = $row->na_dotaz_pres_sms;
		$ret['muze_od'] = $row->muze_od;
		$ret['muze_do'] = $row->muze_do;
		$ret['cas_dojezdu_ke_klientovi_note'] = $row->cas_dojezdu_ke_klientovi_note;
		$ret['cas_dojezdu_do_prahy_note'] = $row->cas_dojezdu_do_prahy_note;
		$ret['divka_je_v_dany_den_v'] = $row->divka_je_v_dany_den_v;
		$ret['note'] = $row->note;
		$ret['date'] = $row->date->getTimestamp();
		return $ret;
	}

	public function saveCalendarWhitNotes($params)
	{
		$id = $params['id'];
		unset($params['id']);
		$date = $params['date'];
		unset($params['date']);

		if (isset($params['online'])) {
			if ($params['online'] == 'true') {
				$params['online'] = 1;
			} else {
				$params['online'] = 0;
			}
		}

		if (isset($params['na_dotaz_zavolanim'])) {
			if ($params['na_dotaz_zavolanim'] == 'true') {
				$params['na_dotaz_zavolanim'] = 1;
			} else {
				$params['na_dotaz_zavolanim'] = 0;
			}
		}
		if (isset($params['na_dotaz_pres_sms'])) {
			if ($params['na_dotaz_pres_sms'] == 'true') {
				$params['na_dotaz_pres_sms'] = 1;
			} else {
				$params['na_dotaz_pres_sms'] = 0;
			}
		}

		$this->db->beginTransaction();

		$caledar = $this->db->table($this->table)->where(array('slecna_id' => $id))->where('date = ?', new \Nette\DateTime($date) . '%');
//		dump($id,$date,new \Nette\DateTime($date),$caledar->count());exit;
//dump($params);exit;
		try {
			if ($caledar->count() == 0) {
				$params['date'] = new \Nette\DateTime($date);
				$params['slecna_id'] = $id;
				$caledar->insert($params);
			} else {
				$caledar->update($params);
			}
			$this->db->commit();
			return TRUE;
		} catch (Exception $e) {
			$this->db->rollBack();
			return FALSE;
		}
	}

	/**
	 * vrati pole timestampu, kdy slecna pracuje nebo nepracuje
	 * timestamp=>boolean 0=nepracuje, 1=pracuje
	 * pole ma jako indexy timestamp_idSlecna
	 * 
	 * @param int $idSlecna \Model\Slecna $slecna->id
	 * @param int $nextDays
	 * @return db\Calendar
	 */
	public function getGirlCalendar2($idSlecna, $nextDays = 14)
	{


		if (is_array($idSlecna)) {
			$ret = array();
			foreach ($idSlecna as $slecna) {
				$slecna = $this->content->slecnaDAO->getByUrlOrIdAndPodnikIds($slecna, $this->content->parameters['podnikid']);
				$slecna = $slecna->id;
				$ret[$slecna] = $this->fillCalendar($slecna, $nextDays);
				unset($c);
			}
			return $ret;
		} else {
			$idSlecna = $this->content->slecnaDAO->getByUrlOrIdAndPodnikIds($idSlecna, $this->content->parameters['podnikid']);
			$idSlecna = $idSlecna->id;
			return $this->fillCalendar($idSlecna, $nextDays);
		}
	}

	private function fillCalendar($idSlecna, $nextDays = 14)
	{
		$calendar = new \App\Model\Calendar;
		foreach ($this->createCalendar($nextDays) as $day) {
			$calendarRow = new CalendarRow();
			$calendarRow->setDate($day);
			$calendar->setRows($calendarRow, $day);
		}

		foreach ($this->db->table($this->table)->select('*')->where(array('slecna_id' => $idSlecna, 'date >= ?' => date('Y-m-d')))->order('date') as $day) {
			$timestamp = $day->date->format('Y-m-d');

			if ($calendar->isSetRow($timestamp)) {
				$calendarRow = $calendar->getRow($timestamp);
				$calendarRow->setSlecna_id($day->slecna_id);
				$calendarRow->setOnline($day->online);
				$calendarRow->setCely_den_online_note($day->cely_den_online_note);
				$calendarRow->setNa_dotaz_zavolanim($day->na_dotaz_zavolanim);
				$calendarRow->setNa_dotaz_pres_sms($day->na_dotaz_pres_sms);
				$calendarRow->setMuze_od($day->muze_od);
				$calendarRow->setMuze_do($day->muze_do);
				$calendarRow->setCas_dojezdu_ke_klientovi_note($day->cas_dojezdu_ke_klientovi_note);
				$calendarRow->setCas_dojezdu_do_prahy_note($day->cas_dojezdu_do_prahy_note);
				$calendarRow->setDivka_je_v_dany_den_v($day->divka_je_v_dany_den_v);
				$calendarRow->setNote($day->note);
				$calendar->setRows($calendarRow, $timestamp);
			}
		}
		//Unsetneme vsetky nepouzite hodnoty
		$calendar->unsetValues();

		return $calendar;
	}

	/**
	 * vrati pole ID slecen, ktere jsou pro zadany den online
	 * @param Nette\DateTime $date
	 * @param array $podnikIds id podniku pro ktere pozaduju online dny,array $podnikIds
	 */
	public function getGirlsCalendarForDay(Nette\DateTime $date)
	{
		$ret = array();
		foreach ($this->db->table('slecnacalendar')->where('date', $date) as $slecna) {
			$ret[$slecna->slecna_id] = new CalendarRow($slecna->toArray());
		}

		return $ret;
	}

}

<?php

namespace App\dao;


/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class SlecnaDAO extends BaseDAO
{

	public $table = 'slecna';
	public $model = 'App\Model\Slecna';
	public $slecna = array();
	protected $user;

	public function __construct(\Nette\Database\Context $db, \Nette\DI\Container $content, \Nette\Security\User $user)
	{
		parent::__construct($db, $content);
		$this->user = $user;
	}

	public function getAll(array $where = array(), $order = null)
	{
		$girls = array();
		foreach ($this->db->table($this->table)->where($where)->order('ord') as $row) {
			$girls[$row->id] = new $this->model($row->toArray());
		}

		foreach ($girls as $key => $value) {
			if (!$this->exist($key)) {
				$girls[$key] = $this->loadEntities($value);
				$this->{$this->table}[$key] = $girls[$key];
			} else {
				$girls[$key] = $this->{$this->table}[$key];
			}
		}
		return $girls;
	}

	public function getAllActive()
	{
		$girls = parent::getAllActive();
		foreach ($girls as $key => $value) {
			if (!$this->exist($key)) {
				$girls[$key] = $this->loadEntities($value);
				$this->{$this->table}[$key] = $girls[$key];
			} else {
				$girls[$key] = $this->{$this->table
								}[$key];
			}
		}
		return $girls;
	}

	public function getAllActiveByPodnikIds(array $podnikIds, $fullEntities = true, $order = 'ord', $modelOrd = null)
	{
		$girls = array();
		$q = $this->db->table($this->table)->select('slecna.*')->where(array('active' => true, 'approved' => true))->where('podnik_id', $podnikIds)->order($order);
		foreach ($q as $row) {
			if (!$this->exist($row->id)) {
				$girls[$row->id] = new $this->model($row->toArray());
				$girls[$row->id] = $this->loadEntities($girls[$row->id], $fullEntities);
				$this->{$this->table
								}[$row->id] = $girls[$row->id];
			} else {
				$girls[$row->id] = $this->{$this->table}[$row->id];
			}
		}
		if ($modelOrd) {
			usort($girls, array($this,'sortBy' . $modelOrd));
		}
		
		return $girls;
	}
	
	public function sortByjmeno($o1,$o2){
		return strcasecmp($o1->getLang('cs')->jmeno, $o2->getLang('cs')->jmeno);
	}

	public function get($id = null, array $where = null, $useCache = true)
	{
		if (!$this->exist($id) || !$useCache) {
			$girl = parent::get($id, $where);
			if ($girl instanceof \App\Model\Slecna) {
				$girl = $this->loadEntities($girl);
				$this->{$this->table}[$id] = $girl;
				return $this->{$this->table}[$id];
			} else {
				return null;
			}
		} else {
			return $this->{$this->table}[$id];
		}
	}

	/**
	 * nahraje do modelu ostatni entity
	 * @param \App\Model\Slecna $slecna
	 * @return \App\Model\Slecna
	 */
	private function loadEntities(\App\Model\Slecna $slecna, $full = true)
	{
		$slecna->setContext($this->content);

		return $slecna;
	}

	public function getByUrlOrIdAndPodnikIds($urlOrId, array $podnikIds)
	{
		if (is_numeric($urlOrId)) {
			$s = $this->db->table($this->table)->where(array('id' => $urlOrId, 'active' => true, 'podnik_id' => $podnikIds))->select('id')->limit(1)->fetch();
		} else {
			$s = $this->db->table($this->table)->where(array('url' => $urlOrId, 'active' => true, 'podnik_id' => $podnikIds))->select('id')->limit(1)->fetch();
		}
		if ($s) {
			$id = $s->id;
		} else {
			throw new \Nette\OutOfRangeException();
		}
		return $this->get($id);
	}

	/**
	 * vrati pole IDcek poskytovanych sluzeb danou slecnou
	 * @param int $slecna_id
	 * @return array
	 */
	public function getGirlOfferedServices(\App\Model\Slecna $slecna)
	{
		$services = array();
		foreach ($this->db->table('slecnaservice')->where(array('slecna_id' => $slecna->id, 'service.servicetype_id' => array_keys($slecna->getServiceTypes()))) as $row) {
			$services[] = $row->service_id;
		}
		return $services;
	}

	public function save(\App\Model\Slecna $model)
	{
		if ($model->id) {
			$oldSlecna = $this->get($model->id, null, false);
			$diff = $model->compareObject($oldSlecna);
		}
		$data = array(
				'inserted' => $model->getInserted(),
				'url' => $model->getUrl(),
				'mainfoto' => $model->getMainfoto(),
				'mainfotoland' => $model->getMainfotoland(),
				'active' => $model->getActive(),
				'approved' => $model->getApproved(),
				'verified' => $model->getVerified(),
				'contact_verified' => $model->getContact_verified(),
				'ord' => $model->getOrd(),
				'podnik_id' => $model->getPodnik_id(),
				'specialprice' => $model->getSpecialprice(),
				'multiprofil' => $model->getMultiprofil(),
				'multi1' => $model->getMulti1(),
				'multi2' => $model->getMulti2(),
				'multi3' => $model->getMulti3(),
				'multi4' => $model->getMulti4(),
				'photo1' => $model->getPhoto1(),
				'photo2' => $model->getPhoto2(),
				'photo3' => $model->getPhoto3(),
				'photo4' => $model->getPhoto4(),
				'photo5' => $model->getPhoto5(),
		);

		$model = parent::saveData($data, $model);
		/* ulozeni jazykovych mutaci */

		foreach ($model->langs as $lang) {
			$lang->setSlecna_id($model->id);
			$this->content->slecnaLangDAO->save($lang);
		}
		/* ulozeni cen */
		if ($model->specialprice == 1) {
			$this->content->priceDAO->save($model->price_privat);
			$this->content->priceDAO->save($model->price_escort_in_prague);
			$this->content->priceDAO->save($model->price_escort_out_prague);
			$this->content->priceDAO->save($model->price_escort_out_prague_more31);
		} else {
			$this->content->priceDAO->deleteForSlecna($model);
		}
		/* ulozeni sluzeb co nabizi */
		$this->saveOfferedServices($model);

		if (isset($diff) && count($diff) > 0) {
			$this->content->changelogDAO->saveData(array(
					'slecna_id' => $model->id,
					'type' => \App\Model\Changelog::TYPE_SLECNA,
					'log' => \Nette\Utils\Json::encode($diff),
					'datetime' => new \Nette\DateTime,
					'user_id' => $this->user->getId()
							), new \App\Model\Changelog());
		}
		return $model;
	}

	private function saveOfferedServices(\App\Model\Slecna $model)
	{
		$insert = array();
		$this->db->table('slecnaservice')->where('slecna_id', $model->id)->delete();

		foreach ($model->serviceTypes as $serviceType) {
			foreach ($serviceType->services as $service) {
				if ($service->getOffered()) {
					$insert[] = array('slecna_id' => $model->id, 'service_id' => $service->id);
				}
			}
		}
		if (count($insert) > 0) {
			$this->db->table('slecnaservice')->insert($insert);
		}
	}

	public function savePhotoPositionByArray($id, $tosave)
	{
		$original = array();
		foreach ($this->db->table('slecnaphoto')->where('slecna_id', $id)->order('ord') as $_photo) {
			$original[$_photo->id] = $_photo->ord;
		}
		foreach ($this->compareTwoArrayValues($tosave, $original) as $idphoto) {
			$this->db->table('slecnaphoto')->where(array('id' => $idphoto, 'slecna_id' => $id))->update(array('ord' => $tosave[$idphoto]));
		}
	}

	public function compareTwoArrayValues(array $array1, array $array2)
	{
		$diff = array();
		foreach ($array2 as $id => $ord) {
			if ($array1[$id] != $ord) {
				$diff[] = $id;
			}
		}
		return $diff;
	}

	/**
	 * vytvori duplikat profilu
	 */
	public function createDuplicate($slecna_id)
	{
		$slecna = $this->get($slecna_id);
		$slecna->id = null;
//		$slecna->url.= '-kopie';
		$slecna->url = $this->getGeneratedUrl($slecna->getLang('cs')->jmeno, '');
		$slecna->photos = array();
		$slecna->photo1 = null;
		$slecna->photo2 = null;
		$slecna->photo3 = null;
		$slecna->photo4 = null;
		$slecna->photo5 = null;
		$slecna->ord = $this->getLastPosition();
		$slecna->specialprice = false;
		foreach ($slecna->langs as $lang) {
			$lang->id = null;
		}
		$slecna = $this->save($slecna);
		return $slecna;
	}

	public function getAllByPrivate()
	{
		$return = array();
		$podniky = new PodnikDAO($this->db, $this->content);
		$podniky = $podniky->getAllFetchPairs('id', 'name');
		foreach ($podniky as $id => $podnik) {
			$return[$id] = array(
					'name' => $podnik,
					'podnik_id' => $id,
					'slecny' => array()
			);
		}

		foreach ($this->getAll() as $slecna) {
			$return[$slecna->getPodnik_id()]['slecny'][] = $slecna;
		}

		return $return;
	}

	public function setLast($slecna_id)
	{
		$slecna = $this->get($slecna_id);
		$maxOrd = $this->db->table($this->table)->max('ord');
		$slecna->ord = $maxOrd + 10;
		$slecna = $this->save($slecna);
		return $slecna;
	}

	public function getLastPosition()
	{
		$maxOrd = $this->db->table($this->table)->max('ord');
		return $maxOrd + 10;
	}

	/**
	 * porovna, zdali jmeno a aktualni url souhlasi. pokud ne, vygeneruje se nove. nejdrive zkousi url samozneho jmena, pokud je obsazeno, pridata postfix -1, -2 atd az najde volnou url.
	 * @param string $name
	 * @param string $actualUrl
	 * @return string|NULL url
	 */
	public function getGeneratedUrl($name, $actualUrl)
	{
		if (empty($name)) {
			return NULL;
		} else {
			$name = \Nette\Utils\Strings::webalize($name);
			if ($actualUrl === NULL || strpos($actualUrl, $name) === FALSE) {
				try {
					$urls = $this->getAll(array('url LIKE ?' => '%' . $name . '%'));
					if (count($urls) == 0) {
						$actualUrl = $name;
					} else {
						$postnumber = 1;
						$regenerate = true;
						while ($regenerate) {
							$searched = false;
							$postfix = '-' . $postnumber;
							foreach ($urls as $url) {
								if ($url->url == $name . $postfix) {
									$searched = true;
								}
							}
							if ($searched) {
								$postnumber++;
							} else {
								$actualUrl = $name . $postfix;
								$regenerate = false;
							}
						}
					}
				} catch (\Nette\ArgumentOutOfRangeException $e) {
					$actualUrl = $name;
				}
			}
			return $actualUrl;
		}
	}

}

<?php

namespace App\dao;

/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class ServiceTypeDAO extends BaseDAO {

	public $table = 'servicetype';
	public $model = 'App\Model\ServiceType';

	public function getAll(array $where = array(),$order = null) {
		$servcies = parent::getAll();
		foreach ($servcies as $key => $value) {
			$value->setServices($this->content->serviceDAO->getAllByServiceType($value->id));
		}
		return $servcies;
	}

	public function getAllByPodnik($podnikId) {
		$ret = array();
		if ($podnikId) {
		foreach ($this->db->table($this->table)->where(':podnik_servicetype.podnik_id = ?', $podnikId)->fetchPairs(null, 'id') as $id) {
			$ret[$id] = $this->get($id);
		}
		}
		return $ret;
	}

	public function get($id = null, array $where = null) {
		$service = parent::get($id, $where);
		$service->setServices($this->content->serviceDAO->getAllByServiceType($service->id));
		return $service;
	}

	/**
	 * Vrati vsechny podniky fetchnute do pole
	 * @param string $key
	 * @param string $value
	 * @return array
	 */
	public function getAllFetchPairsByPodnik($podnikId, $key, $value) {
		return $this->db->table($this->table)->where(':podnik_servicetype.podnik_id = ?', $podnikId)->fetchPairs($key, $value);
	}

}

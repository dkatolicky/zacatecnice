<?php

namespace App\dao;


/**
 * Description of NovinkyDAO
 *
 * @author David
 */
class ChangelogDAO extends BaseDAO
{

	public $table = 'changelog';
	public $model = 'App\Model\Changelog';

	public function getAllForProfile(array $where = array(), $order = null, $limit = 3)
	{
		$ret = array();
		$rows = $this->db->table($this->table)->where($where);
		if ($order) {
			$rows->order($order);
		}
		foreach ($rows->limit($limit) as $row) {
			$ret[$row->id] = new $this->model($row->toArray());
		}
		return $ret;
	}

	public function confirmByAdmin($id)
	{
		$this->db->table($this->table)->where('id', $id)->update(array('confirm_admin' => 1));
	}

	public function confirmBySubadmin($id)
	{
		$this->db->table($this->table)->where('id', $id)->update(array('confirm_subadmin' => 1));
	}

	public function getDateOfLastModification($slecnaId, $type)
	{
		$x = $this->db->table($this->table)->where(array('slecna_id'=> $slecnaId,'type'=>$type))->order('datetime DESC')->limit(1)->fetch();
		if ($x) {
			return $x->datetime;
		} else {
			return null;
		}
	}

}

<?php

namespace App\Facade;

use Nette\Object;

/**
 * Description of SlecnaFacade
 *
 * @author Pali Bazant <pali.bazant@gmail.com>
 */
class SlecnaFacade extends Object {

	/** @var \App\dao\CalendarDAO */
	private $calendarDAO;

	public function __construct(\App\dao\CalendarDAO $calendarDAO) {
		$this->calendarDAO = $calendarDAO;
	}

	/**
	 * Upravy data a ulozi do DB
	 * @param type $post
	 * @param type $get
	 * @return boolean
	 */
	public function saveCalendar($post, $get = NULL) {
		$ret = FALSE;
		if (isset($get['oneRow'])) {
			if (isset($post['muze_od']) && $post['muze_od'] != '') {
				list($hour, $minute) = explode(':', $post['muze_od']);
				$post['muze_od'] = mktime($hour, $minute, 0, date('d'), date('m'), date('Y'));
			}
			if (isset($post['muze_do']) && $post['muze_do'] != '') {
				list($hour, $minute) = explode(':', $post['muze_do']);
				$post['muze_do'] = mktime($hour, $minute, 0, date('d'), date('m'), date('Y'));
			}
			if ($this->calendarDAO->saveCalendarWhitNotes($post)) {
				$ret = TRUE;
			} else {
				$ret = FALSE;
			}
		} else {
			foreach ($post as $row) {
				if (isset($post['muze_od']) && $row['muze_od'] != '') {
					list($hour, $minute) = explode(':', $row['muze_od']);
					$row['muze_od'] = mktime($hour, $minute, 0, date('d'), date('m'), date('Y'));
				}
				if (isset($post['muze_do']) && $row['muze_do'] != '') {
					list($hour, $minute) = explode(':', $row['muze_do']);
					$row['muze_do'] = mktime($hour, $minute, 0, date('d'), date('m'), date('Y'));
				}
				if ($this->calendarDAO->saveCalendarWhitNotes($row)) {
					$ret = TRUE;
				} else {
					$ret = FALSE;
				}
			}
		}

		return $ret;
	}

}

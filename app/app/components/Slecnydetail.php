<?php

namespace App\Components;

use Nette;

/**
 * Description of leftMenu
 *
 * @author David
 */
class Slecnydetail extends BaseComponent{

	public function render() {
		parent::render();
		$template = $this->template;
		$this->template->setFile($this->getPresenter()->context->parameters['appDir'] . '/templates/components/slecnydetail.latte');
		$this->template->lang = $this->getPresenter()->getParameter('lang');
		$this->template->slecny = $this->getPresenter()->context->getService('slecnaDAO')->getAllActiveByPodnikIds($this->getPresenter()->context->parameters['podnikid']);
		$template->render();
	}

}

<?php

namespace App\Presenters;

/**
 * Description of PagesPresenter
 *
 * @author David
 */
class PagesPresenter extends BasePresenter {
	private $kalendar;
	private $kalendar2;
	public function __construct(\App\Components\Kalendar $kalendar,  \App\Components\Kalendar2 $kalendar2) {
		parent::__construct();
		$this->kalendar = $kalendar;
		$this->kalendar2 = $kalendar2;
	}

	public function actionDefault($lang, $item) {
		if ($lang == 'cs') {
			$this->template->postfix = '';
		} else {
			$this->template->postfix = '_' . $lang;
		}
		
	}
	public function beforeRender() {
		parent::beforeRender();
		
	}

	public function renderDefault($lang, $item) {
		$this->template->setFile($this->getTemplatePath("full-width"));
		$this->template->menu = $this->menuDAO->getByWebIdAndItem($this->context->parameters['webid'], $item);
//		$content = $this->page->getContent($item, $lang);
////    dump($content);exit;
//		if ($content) {
//			$this->template->content = $content;
//		} else {
//			$this->redirect("Pages:default");
//		}
	}

	/**
	 * Vraci celou cestu k template + jeji nazev
	 * @param string $template nazev sablony
	 * @return string
	 */
	private function getTemplatePath($template) {
		return $this->context->parameters["appDir"] . "/templates/Pages/" . $template . ".latte";
	}
	
	
	
	
	
	
	
	/**
	 * Formular pro rezervaci
	 * 
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentReservation(){
		
		$form = new \Nette\Application\UI\Form;
		$form->setTranslator(new \App\Model\MyTranslator($this->getParam('lang')));
		$form->addText('jmeno', 'Jméno, kterým Tě máme oslovovat')->setAttribute('class', 'myInput');
		$form->addText('tel', 'Tvůj kontaktní telefon')
						->setRequired('Kontaktní telefon je povinná položka, prosím vyplňte ji.')
						->setAttribute('class', 'myInput');
		$form->addText('email', 'Tvůj kontaktní e-mail')->setAttribute('class', 'myInput');
		$form->addText('datum', 'Datum, kdy máš zájem o jednu z nás')->setAttribute('class', 'myInput datepicker');
		$form->addText('prijezd', 'Čas, kdy máme přijet')->setAttribute('class', 'myInput timepicker');
		$form->addSelect('delka', 'Předpoklad délky společného času', array(
				'1 hodina' => '1 hodina', '2 hodiny' => '2 hodiny', '3 hodiny' => '3 hodiny', '4 hodiny' => '4 hodiny', '5 hodin' => '5 hodin', 
				'6 hodin' => '6 hodin', '12 hodin' => '12 hodin', '1 den' => '1 den', '2 dny' => '2 dny', '3 dny' => '3 dny', '4 dny' => '4 dny', 
				'5 dní' => '5 dní', '6 dní' => '6 dní', '7 dní' => '7 dní'));
		$form->addText('preferSlecna', 'Preferovaná slečna, kterou by jsi nejraději')->setAttribute('class', 'myInput');
		$form->addText('preferSlecna2', 'Pokud v uvedeném čase nebude slečna moct, uveď prosím jméno jiné preferované slečny/slečen')->setAttribute('class', 'myInput');
		$form->addText('adresa', 'Adresa, kam máme přijet')->setAttribute('class', 'myInput');
		$form->addSelect('houseType', 'Doprovod je k Tobě na', array('Hotel' => 'Hotel', 'Byt' => 'Byt', 'Dům' => 'Dům'));
		$form->addTextArea('poznamky', 'Doplňující poznámky a požadavky')->setAttribute('class', 'myInput');
		
		$form->addSubmit('save', 'Rezervuj')->setAttribute('class', 'button myButton');
		$form->onSuccess[] = $this->sendMailReservationSucceded;
		
		return $form;
	}
	
	
	/**
	 * Spracovava formular pro rezervaci a odesila email
	 * @param \Nette\Application\UI\Form $form
	 */
	public function sendMailReservationSucceded(\Nette\Application\UI\Form $form){
		$values = $form->getValues();
		$this->template->setFile($this->getTemplatePath("forms/reservation.email"));
		$this->template->values = $values;
		$mail = new \Nette\Mail\Message();
		$mail->setFrom('rezervace@zacatecnice.cz')
    ->addTo('rezervace@zacatecnice.cz')
    ->setSubject('Rezervace: vyplněn formulář')
    ->setHTMLBody($this->template->__toString());
		$sender = new \Nette\Mail\SendmailMailer();
		$sender->send($mail);
		$this->flashMessage('Vaše rezervace byla odeslána.');
		$this->redirect('Pages:default', array('item' => $this->getPresenter()->getParam('item'), 'lang' => $this->getPresenter()->getParam('lang')));
	}
	
	/**
	 * Formular pro informace pro VIP
	 * 
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentInfoForVip(){
		$form = new \Nette\Application\UI\Form;
		$form->setTranslator(new \App\Model\MyTranslator($this->getParam('lang')));
		$form->addText('jmeno', 'Jméno, kterým Tě máme oslovovat')->setAttribute('class', 'myInput');
		$form->addText('tel', 'Tvůj kontaktní telefon')
						->setRequired('Kontaktní telefon je povinná položka, prosím vyplňte ji.')
						->setAttribute('class', 'myInput');
		$form->addText('email', 'Tvůj kontaktní e-mail')->setAttribute('class', 'myInput');
		$form->addTextArea('poznamky', 'Doplnující poznámky a požadavky')->setAttribute('class', 'myInput');
		
		$form->addSubmit('save', 'Odešli')->setAttribute('class', 'button myButton');
		$form->onSuccess[] = $this->sendMailInfoForVipSucceded;
		
		return $form;
	}
	
	
	/**
	 * Spracovava formular pro informace pro VIP a edesila email
	 * @param \Nette\Application\UI\Form $form
	 */
	public function sendMailInfoForVipSucceded(\Nette\Application\UI\Form $form){
		$values = $form->getValues();
		$this->template->setFile($this->getTemplatePath("forms/infoForVip.email"));
		$this->template->values = $values;
		$mail = new \Nette\Mail\Message();
		$mail->setFrom('form@zacatecnice.cz')
    ->addTo('form@zacatecnice.cz')
    ->setSubject('Informace pro VIP: vyplněn formulář')
    ->setHTMLBody($this->template->__toString());
		$sender = new \Nette\Mail\SendmailMailer();
		$sender->send($mail);
		$this->flashMessage('Váš požadavek byl zaznamenán.');
		$this->redirect('Pages:default', array('item' => $this->getPresenter()->getParam('item'), 'lang' => $this->getPresenter()->getParam('lang')));
	}
	
	/**
	 * Formular pro informace pro VIP
	 * 
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentContact(){
		$form = new \Nette\Application\UI\Form;
		$form->setTranslator(new \App\Model\MyTranslator($this->getParam('lang')));
		$form->addText('jmeno', 'Jméno, kterým Tě máme oslovovat')->setAttribute('class', 'myInput');
		$form->addText('tel', 'Tvůj kontaktní telefon')
						->setRequired('Kontaktní telefon je povinná položka, prosím vyplňte ji.')
						->setAttribute('class', 'myInput');
		$form->addText('email', 'Tvůj kontaktní e-mail')->setAttribute('class', 'myInput');
		$form->addTextArea('poznamky', 'Doplnující poznámky a požadavky')->setAttribute('class', 'myInput');
		
		$form->addSubmit('save', 'Odešli')->setAttribute('class', 'button myButton');
		$form->onSuccess[] = $this->sendMailContactSucceded;
		
		return $form;
	}
	
	
	/**
	 * Spracovava formular pro informace pro VIP a edesila email
	 * @param \Nette\Application\UI\Form $form
	 */
	public function sendMailContactSucceded(\Nette\Application\UI\Form $form){
		$values = $form->getValues();
		$this->template->setFile($this->getTemplatePath("forms/infoForVip.email"));
		$this->template->values = $values;
		$mail = new \Nette\Mail\Message();
		$mail->setFrom('divky@zacatecnice.cz')
    ->addTo('divky@zacatecnice.cz')
    ->setSubject('Kontakt: vyplněn formulář')
    ->setHTMLBody($this->template->__toString());
		$sender = new \Nette\Mail\SendmailMailer();
		$sender->send($mail);
		$this->flashMessage('Váš požadavek byl zaznamenán.');
		$this->redirect('Pages:default', array('item' => $this->getPresenter()->getParam('item'), 'lang' => $this->getPresenter()->getParam('lang')));
	}
	
	/**
	 * Formular pro prace v erotice
	 * 
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentWorkInErotic(){
		$form = new \Nette\Application\UI\Form;
		$form->setTranslator(new \App\Model\MyTranslator($this->getParam('lang')));
		$form->addText('jmeno', 'Jméno')->setAttribute('class', 'myInput')->setRequired('Vplňte prosím jméno');
		$form->addText('jmenoPracovni', 'Jméno - pracovní')->setAttribute('class', 'myInput');
		$form->addText('email', 'E-mail')->setAttribute('class', 'myInput');
		$form->addText('tel', 'Telefon')
						->setRequired('Kontaktní telefon je povinná položka, prosím vyplňte ji.')
						->setAttribute('class', 'myInput');
		$form->addText('odkud', 'Odkud jsi')->setAttribute('class', 'myInput');
		$form->addText('vek', 'Věk')->setAttribute('class', 'myInput');
		$form->addText('vyska', 'Výška')->setAttribute('class', 'myInput');
		$form->addText('vaha', 'Váha')->setAttribute('class', 'myInput');
		$form->addText('prsa', 'Prsa')->setAttribute('class', 'myInput');
		$form->addText('vlasy', 'Vlasy')->setAttribute('class', 'myInput');
		$form->addText('oci', 'Oči')->setAttribute('class', 'myInput');
		$form->addText('jazyky', 'Jazyky')->setAttribute('class', 'myInput');
		$form->addText('zkusenosti', 'Zkušenosti')->setAttribute('class', 'myInput');
		$form->addTextArea('poznamky', 'Popis, poznámka')->setAttribute('class', 'myInput');
		$form->addUpload('uploadedFile_1')->setAttribute('class', 'myInput');
		$form->addUpload('uploadedFile_2')->setAttribute('class', 'myInput');
		$form->addUpload('uploadedFile_3')->setAttribute('class', 'myInput');
		$form->addUpload('uploadedFile_4')->setAttribute('class', 'myInput');
		$form->addUpload('uploadedFile_5')->setAttribute('class', 'myInput');
		$form->addUpload('uploadedFile_6')->setAttribute('class', 'myInput');
		
		$form->addSubmit('save', 'Odešli')->setAttribute('class', 'button myButton');
		$form->onSuccess[] = $this->sendMailWorkInEroticSucceded;
		
		return $form;
	}
	
	
	/**
	 * Spracovava formular pro prace v erotice + odeslani emailu
	 * @param \Nette\Application\UI\Form $form
	 */
	public function sendMailWorkInEroticSucceded(\Nette\Application\UI\Form $form){
		$values = $form->getValues();
		$params = $this->getContext()->getParameters();
		$noveFotky = array();
		for ($i = 1; $i <= 6; $i++) {
			$_n = 'uploadedFile_' . $i;
			if ($values->$_n->isOk() === true && $values->$_n->isImage() === true) {
				$noveFotky[] = $this->saveFile($values->$_n, $params['photoDir']);
			}
		}
		$this->template->setFile($this->getTemplatePath("forms/workInErotic.email"));
		$this->template->values = $values;
		$this->template->photos = $noveFotky;
		if(isset($values->email) && $values->email != ''){
			$from = $values->email;
		}else{
			$from = 'profil@zacatecnice.cz';
		}
		$mail = new \Nette\Mail\Message();
		$mail->setFrom($from)
    ->addTo('profil@zacatecnice.cz')
    ->addBcc('pali.bazant@gmail.com')
    ->addBcc('dkatolicky@gmail.com')
    ->setSubject('Práce v erotice: vyplněn formulář')
    ->setHTMLBody($this->template->__toString());
		$sender = new \Nette\Mail\SendmailMailer();
		$sender->send($mail);
		$this->flashMessage('Váš požadavek byl zaznamenán.');
		$this->redirect('Pages:default', array('item' => $this->getPresenter()->getParam('item'), 'lang' => $this->getPresenter()->getParam('lang')));
	}
	
	public function createComponentKalendar(){
		return $this->kalendar;
	}
	public function createComponentKalendar2(){
		return $this->kalendar2;
	}

}

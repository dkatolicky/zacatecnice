<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;


$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
				->addDirectory(__DIR__)
				->addDirectory(__DIR__ . '/../vendor/others')
				->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
if (in_array($_SERVER["SERVER_NAME"], App\RouterFactory::$productionServers)) {
	$configurator->setDebugMode(false); // enable for your remote IP
	$configurator->addConfig(__DIR__ . '/config/config.production.neon');
} else {
	$configurator->setDebugMode(TRUE); // enable for your remote IP
	$configurator->addConfig(__DIR__ . '/config/config.local.neon');
}

$configurator->enableDebugger(__DIR__ . '/../log', 'dkatolicky@gmail.com');

$container = $configurator->createContainer();

return $container;

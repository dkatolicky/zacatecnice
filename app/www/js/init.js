$(document).ready(function() {
	
	
	$("input.focused").focus();
	
	
	$("img.img1").mouseenter(function() {
		$(this).animate({
			width: "110%",
			height: "110%",
			opacity: 1,
			'margin-left': "-5%",
			'margin-top': "-5%"
		}, 100);
	}).mouseout(function() {
		$(this).animate({
			width: "100%",
			height: "100%",
			opacity: 1,
			'margin-left': 0,
			'margin-top': 0
		}, 50);
	});
	

});
<?php

namespace Emailer;

use Nette\Application\UI\Control;
use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Nette\Http\Request;

class Emailer extends Control {

	/** @var string */
	protected $defaultFromEmail;

	/** @var string */
	protected $defaultToEmail;

	/** @var IMailer */
	protected $mailer;

	/** @var boolean */
	protected $testing;

	public function __construct(IMailer $mailer, $defaultFromEmail, $defaultToEmail = null, $testing = true) {

		$this->testing = $testing;
		$this->defaultFromEmail = $defaultFromEmail;
		$this->defaultToEmail = $defaultToEmail;
		$this->mailer = $mailer;
	}

//	public function setPresenter($presenter){
//		parent::__construct($presenter);
//	}

	public function getTemplateFilePath($templateFileName) {
		return __DIR__ . '/templates/' . $templateFileName . '.latte';
	}

	public function send($templateFileName, $subject, $toEmail = null, $fromEmail = null, $htmlBody = null, $attachments = array()) {
		$_toEmail =$toEmail;
		$message = new Message();
		if ($fromEmail === null) {
			$fromEmail = $this->defaultFromEmail;
		}
		if (($toEmail === null) || ($this->testing == true)) {
			$toEmail = $this->defaultToEmail; // don't spam the real e-mails when in the development or testing mode
		}
		if (is_string($toEmail)) {
			if (\Nette\Utils\Validators::isEmail($toEmail)) {
				$message->addTo($toEmail);
			}
		} elseif (is_array($toEmail)) {
			foreach ($toEmail as $email) {
				if (\Nette\Utils\Validators::isEmail($email)) {
					$message->addTo($email);
				}
			}
		} else {
			throw new \Nette\InvalidArgumentException('Unexpected mail address format. Address must be string or array.');
		}
		foreach ($attachments as $attachment) {
			$message->addAttachment($attachment);
		}
		if ($this->testing == true) {
			$subject .= ' (T - '.json_encode($_toEmail).')';
		}
		$this->template->htmlBody = ($htmlBody !== null ? nl2br($htmlBody) : null);
		$this->template->subject = $subject;
		$this->template->testing = $this->testing;

		$this->template->setFile($this->getTemplateFilePath($templateFileName));
		$message->setFrom($fromEmail)
						->setSubject($subject)
						->setHtmlBody($this->template);

		return $this->mailer->send($message);
	}

}
